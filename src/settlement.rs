#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Settlement {
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    #[prost(int64, tag = "2")]
    pub id: i64,
    #[prost(int64, tag = "3")]
    pub player_id: i64,
    #[prost(message, optional, tag = "4")]
    pub pos: ::core::option::Option<Position>,
    #[prost(message, optional, tag = "5")]
    pub depot: ::core::option::Option<Depot>,
    #[prost(message, repeated, tag = "6")]
    pub buildings: ::prost::alloc::vec::Vec<Building>,
    #[prost(message, repeated, tag = "7")]
    pub build_queue: ::prost::alloc::vec::Vec<BuildQueueItem>,
    #[prost(int64, tag = "8")]
    pub build_queue_size: i64,
    #[prost(message, optional, tag = "9")]
    pub last_update: ::core::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BuildQueueItem {
    #[prost(int64, tag = "1")]
    pub building_id: i64,
    #[prost(message, optional, tag = "2")]
    pub build_start: ::core::option::Option<::prost_types::Timestamp>,
    #[prost(message, optional, tag = "3")]
    pub build_end: ::core::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Position {
    #[prost(int64, tag = "1")]
    pub x: i64,
    #[prost(int64, tag = "2")]
    pub y: i64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Depot {
    #[prost(message, repeated, tag = "1")]
    pub store: ::prost::alloc::vec::Vec<ResourceStorage>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ResourceStorage {
    #[prost(int64, tag = "1")]
    pub capacity: i64,
    #[prost(int64, tag = "2")]
    pub current_stored: i64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SettlementList {
    #[prost(message, repeated, tag = "1")]
    pub settlements: ::prost::alloc::vec::Vec<Settlement>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Building {
    #[prost(int64, tag = "1")]
    pub id: i64,
    #[prost(string, tag = "3")]
    pub build_def: ::prost::alloc::string::String,
    #[prost(bool, tag = "4")]
    pub in_construction: bool,
    #[prost(bool, tag = "5")]
    pub hibernated: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BuildBuildingCall {
    #[prost(message, optional, tag = "1")]
    pub settlement_id: ::core::option::Option<super::common::SettlementId>,
    #[prost(string, tag = "2")]
    pub build_def: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpgradeBuildingCall {
    #[prost(message, optional, tag = "1")]
    pub settlement_id: ::core::option::Option<super::common::SettlementId>,
    #[prost(message, optional, tag = "2")]
    pub building_id: ::core::option::Option<super::common::BuildingId>,
}
#[doc = r" Generated client implementations."]
pub mod settlement_services_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct SettlementServicesClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl SettlementServicesClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> SettlementServicesClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn add_settlement(
            &mut self,
            request: impl tonic::IntoRequest<super::Settlement>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/AddSettlement",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn add_test_settlement(
            &mut self,
            request: impl tonic::IntoRequest<super::Settlement>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/AddTestSettlement",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_settlement_by_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::SettlementId>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/GetSettlementByID",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_settlements_by_user_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::SettlementList>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/GetSettlementsByUserID",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete_settlement(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::SettlementId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/DeleteSettlement",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn build_building(
            &mut self,
            request: impl tonic::IntoRequest<super::BuildBuildingCall>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/BuildBuilding",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn upgrade_building(
            &mut self,
            request: impl tonic::IntoRequest<super::UpgradeBuildingCall>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/settlement.SettlementServices/UpgradeBuilding",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for SettlementServicesClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for SettlementServicesClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "SettlementServicesClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod settlement_services_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with SettlementServicesServer."]
    #[async_trait]
    pub trait SettlementServices: Send + Sync + 'static {
        async fn add_settlement(
            &self,
            request: tonic::Request<super::Settlement>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status>;
        async fn add_test_settlement(
            &self,
            request: tonic::Request<super::Settlement>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status>;
        async fn get_settlement_by_id(
            &self,
            request: tonic::Request<super::super::common::SettlementId>,
        ) -> Result<tonic::Response<super::Settlement>, tonic::Status>;
        async fn get_settlements_by_user_id(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::SettlementList>, tonic::Status>;
        async fn delete_settlement(
            &self,
            request: tonic::Request<super::super::common::SettlementId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
        async fn build_building(
            &self,
            request: tonic::Request<super::BuildBuildingCall>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
        async fn upgrade_building(
            &self,
            request: tonic::Request<super::UpgradeBuildingCall>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct SettlementServicesServer<T: SettlementServices> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: SettlementServices> SettlementServicesServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for SettlementServicesServer<T>
    where
        T: SettlementServices,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/settlement.SettlementServices/AddSettlement" => {
                    #[allow(non_camel_case_types)]
                    struct AddSettlementSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices> tonic::server::UnaryService<super::Settlement> for AddSettlementSvc<T> {
                        type Response = super::Settlement;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Settlement>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).add_settlement(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddSettlementSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/AddTestSettlement" => {
                    #[allow(non_camel_case_types)]
                    struct AddTestSettlementSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices> tonic::server::UnaryService<super::Settlement>
                        for AddTestSettlementSvc<T>
                    {
                        type Response = super::Settlement;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Settlement>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).add_test_settlement(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddTestSettlementSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/GetSettlementByID" => {
                    #[allow(non_camel_case_types)]
                    struct GetSettlementByIDSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices>
                        tonic::server::UnaryService<super::super::common::SettlementId>
                        for GetSettlementByIDSvc<T>
                    {
                        type Response = super::Settlement;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::SettlementId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_settlement_by_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetSettlementByIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/GetSettlementsByUserID" => {
                    #[allow(non_camel_case_types)]
                    struct GetSettlementsByUserIDSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices>
                        tonic::server::UnaryService<super::super::common::UserId>
                        for GetSettlementsByUserIDSvc<T>
                    {
                        type Response = super::SettlementList;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).get_settlements_by_user_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetSettlementsByUserIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/DeleteSettlement" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteSettlementSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices>
                        tonic::server::UnaryService<super::super::common::SettlementId>
                        for DeleteSettlementSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::SettlementId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete_settlement(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteSettlementSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/BuildBuilding" => {
                    #[allow(non_camel_case_types)]
                    struct BuildBuildingSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices>
                        tonic::server::UnaryService<super::BuildBuildingCall>
                        for BuildBuildingSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::BuildBuildingCall>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).build_building(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = BuildBuildingSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/settlement.SettlementServices/UpgradeBuilding" => {
                    #[allow(non_camel_case_types)]
                    struct UpgradeBuildingSvc<T: SettlementServices>(pub Arc<T>);
                    impl<T: SettlementServices>
                        tonic::server::UnaryService<super::UpgradeBuildingCall>
                        for UpgradeBuildingSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpgradeBuildingCall>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).upgrade_building(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpgradeBuildingSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: SettlementServices> Clone for SettlementServicesServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: SettlementServices> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: SettlementServices> tonic::transport::NamedService for SettlementServicesServer<T> {
        const NAME: &'static str = "settlement.SettlementServices";
    }
}
