#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum ResourceType {
    Forest = 0,
    Wood = 1,
    Gold = 2,
    Plank = 3,
}
